package com.practice.graphql.controller;

import com.practice.graphql.model.*;
import com.practice.graphql.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class SaveDataController {

    private final EmployeeRepository employeeRepository;
    private final AadharRepository aadharRepository;
    private final StudentRepository studentRepository;
    private final SystemsRepository systemsRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public SaveDataController(EmployeeRepository employeeRepository, AadharRepository aadharRepository, StudentRepository studentRepository, SystemsRepository systemsRepository, VehicleRepository vehicleRepository) {
        this.employeeRepository = employeeRepository;
        this.aadharRepository = aadharRepository;
        this.studentRepository = studentRepository;
        this.systemsRepository = systemsRepository;
        this.vehicleRepository = vehicleRepository;
    }


    public void saveData(int noOfRecords) {

        for (int i = 1; i <= noOfRecords; i++) {
            try {
                Employee employee = new Employee();
                employee.setName("Ramesh Rao " + i);
                employee.setClient("HP " + i);
                employee.setCompany("TCS " + i);
                employee.setContact("123456" + i);
                employee.setCountry("India " + i);
                employee.setEmail("rao9536" + i + "@gmail.com");
                employee.setJobCity("Toronto " + i);
                employee.setSalary(6500 + i);
                employeeRepository.save(employee);

                Aadhar aadhar = new Aadhar();
                aadhar.setAadharNumber("941867521" + i);
                aadhar.setCity("Delhi " + i);
                aadhar.setContact("123456" + i);
                aadhar.setCountry("India " + i);
                aadhar.setName("Rao " + i);
                aadharRepository.save(aadhar);

                Student student = new Student();
                student.setName("Ramesh " + i);
                student.setCollege("Lovely " + i);
                student.setContact("6789 " + i);
                student.setCourse("Java " + i);
                student.setEmail("rao9536" + i + "@gmail.com");
                studentRepository.save(student);

                Systems systems = new Systems();
                systems.setModel("Thinkpad " + "470" + i);
                systems.setCompany("Lenovo " + i);
                systems.setPrice(2455 + i);
                systems.setProcessor("i5 " + i);
                systems.setRam("2 GB " + i);
                systemsRepository.save(systems);

                Vehicle vehicle = new Vehicle();
                vehicle.setModel("Mustang " + i);
                vehicle.setCompany("Ford " + i);
                vehicle.setEngine(2500 + i);
                vehicle.setPrice(25000 + i);
                vehicle.setType("Sports " + i);
                vehicleRepository.save(vehicle);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
