package com.practice.graphql.controller;

import com.practice.graphql.model.*;
import com.practice.graphql.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestApiController {

    private static final String SAVE_STUDENT_MSG = " Student object saved successfully";
    private static final String SAVE_AADHAR_MSG = " Aadhar object saved successfully";
    private static final String SAVE_EMPLOYEE_MSG = " Employee object saved successfully";
    private static final String SAVE_SYSTEMS_MSG = " Systems object saved successfully";
    private static final String SAVE_VEHICLE_MSG = " Vehicle object saved successfully";
    private static final String BULK_DATA_MSG = " Data created successfully";
    private static final String DATA_DELETE_MSG = " All the data deleted successfully";


    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private AadharRepository aadharRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SystemsRepository systemsRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private SaveDataController saveDataController;

    @RequestMapping(method = RequestMethod.GET, value = "/studentByName")
    public Student getStudentByName(@RequestParam("name") String name) {
        return studentRepository.getStudentByName(name);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allStudent")
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveStudent")
    public String saveStudent(@RequestBody Student student) {
        studentRepository.save(student);
        return student.getName() + SAVE_STUDENT_MSG;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAadhar")
    public String saveAadhar(@RequestBody Aadhar aadhar) {
        aadharRepository.save(aadhar);
        return aadhar.getName() + SAVE_AADHAR_MSG;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allAadhar")
    public List<Aadhar> allAadhar() {
        return aadharRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveEmployee")
    public String saveEmployee(@RequestBody Employee employee) {
        employeeRepository.save(employee);
        return employee.getName() + SAVE_EMPLOYEE_MSG;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allEmployee")
    public List<Employee> allEmployee() {
        return employeeRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveSystems")
    public String saveSystems(@RequestBody Systems systems) {
        systemsRepository.save(systems);
        return systems.getCompany() + SAVE_SYSTEMS_MSG;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allSystems")
    public List<Systems> allSystems() {
        return systemsRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveVehicle")
    public String saveVehicle(@RequestBody Vehicle vehicle) {
        vehicleRepository.save(vehicle);
        return vehicle.getCompany() + SAVE_VEHICLE_MSG;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/allVehicle")
    public List<Vehicle> allVehicle() {
        return vehicleRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/createBulkData")
    public String createBulkData(@RequestParam("noOfRecords") int noOfRecords) {
        saveDataController.saveData(noOfRecords);
        return BULK_DATA_MSG;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/deleteAllData")
    public String deleteAllData() {
        studentRepository.deleteAll();
        aadharRepository.deleteAll();
        employeeRepository.deleteAll();
        systemsRepository.deleteAll();
        vehicleRepository.deleteAll();
        return DATA_DELETE_MSG;
    }
}
