package com.practice.graphql.repository;

import com.practice.graphql.model.Systems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemsRepository extends JpaRepository<Systems, Long> {
}
