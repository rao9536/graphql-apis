package com.practice.graphql.repository;

import com.practice.graphql.model.Aadhar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AadharRepository extends JpaRepository<Aadhar, Long> {
}
